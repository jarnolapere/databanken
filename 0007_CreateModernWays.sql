USE ModernWays;
CREATE DATABASE cursusDataBanken;
CREATE TABLE Boeken(
Voornaam VARCHAR(50) CHAR SET utf8mb4,
Familienaam VARCHAR(80) CHAR SET utf8mb4,
Titel VARCHAR(255) CHAR SET utf8mb4,
Stad VARCHAR(50) CHAR SET utf8mb4,
Verschijningjaar VARCHAR(4),
Uitgeverij VARCHAR(80) CHAR SET utf8mb4,
Herdruk VARCHAR(4),
Commentaar VARCHAR(1000)
);
USE ModernWays;
ALTER TABLE Boeken DROP COLUMN Commentaar;
USE ModernWays;
ALTER TABLE Boeken ADD COLUMN Commentaar VARCHAR(150) CHAR SET utf8mb4;
USE ModernWays;
ALTER TABLE Boeken CHANGE Familienaam Familienaam VARCHAR(200) CHAR SET utf8mb4 NOT NULL;